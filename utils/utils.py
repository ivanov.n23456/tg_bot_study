from typing import Optional
from database.dataclass import Format
from geopy.geocoders import Nominatim
from timezonefinder import TimezoneFinder
import datetime as dt
import pytz

geolocator = Nominatim(user_agent='deluvremya1234214242')
# авторизация в geopy
obj = TimezoneFinder()
# создаём экземпляр класса


def check_city(city: str) -> bool:
    location = geolocator.geocode(city)
    # поиск города через geopy
    if location:
        return True
    else:
        return False


def get_timezone(city: str) -> Optional[int]:
    """Если знаем его таймзону, отдаем ее, если нет unknown"""
    location = geolocator.geocode(city)
    # поиск города через geopy
    if location:
        tz_name = obj.timezone_at(lng=location.longitude, lat=location.latitude)
        # получение названия часового пояса по координатам
        return int(str(dt.datetime.now(pytz.timezone(tz_name)))[-6:-3])
        # через срезы форматируем часовой пояс, оставляем смещение времени по UTC
    else:
        return False


def format_to_enum(form: str) -> Optional[Format]:
    format_to_enum_dict = {'Очная': Format.LIVE,
                           'Онлайн': Format.ONLINE}
    return format_to_enum_dict.get(form, None)
