from contextlib import suppress

from aiogram import F, Router, types
from aiogram.exceptions import TelegramBadRequest
from aiogram.filters import Command, StateFilter
from aiogram.fsm.context import FSMContext
from aiogram.types import (InlineKeyboardButton, InlineKeyboardMarkup,
                           KeyboardButton, Message, ReplyKeyboardMarkup,
                           ReplyKeyboardRemove)
from aiogram.utils.keyboard import ReplyKeyboardBuilder

from filters.filters import CityExist
from keyboards.keyboards import (TimeCallbackFactory, education_form_kb,
                                 gender_kb, get_time_kb, timezone_kb, yesno_kb)
from states.states import Bot_state
from utils.utils import get_timezone, format_to_enum

from database.repository import user_repo
from database.dataclass import User, Candidate, CandidateStatus

router: Router = Router()

@router.message(Command(commands='del'))
async def erase(message: Message, state: FSMContext):
    """Просто для сброса состояний, на вреся тестов"""
    await state.clear()
    data = await state.get_data()
    print(data)


@router.message(Command(commands='start'))
async def cmd_start(message: Message, state: FSMContext):
    text = 'Мы рады приветствовать в боте Союза Марксистов\n'
    text += 'Этот бот поможет подобрать кружок, в котором можно начать обучение."\n'
    text += 'Для начала необходимо пройти регистрацию'
    await message.answer(text)
    await message.answer("Как Вас зовут?")

    await state.set_state(Bot_state.ask_name)


# TODO исправление регистрационных данных.
@router.message(F.text, StateFilter(Bot_state.ask_name))
async def name_process(message: Message, state: FSMContext):
    await state.update_data(tg_name=message.from_user.username)
    await state.update_data(name=message.text)

    await message.answer(f'Приятно познакомится {message.text}!')
    await message.answer('Сколько тебе лет?')
    await state.set_state(Bot_state.ask_age)


@router.message(~F.text.isdigit(), StateFilter(Bot_state.ask_age))
async def age_process_invalid(message: Message, state: FSMContext):
    await message.answer("Пожалуйста ответь числом")


@router.message(F.text.isdigit(), StateFilter(Bot_state.ask_age))
async def age_process(message: Message, state: FSMContext):
    await state.update_data(age=int(message.text))
    await message.answer("Из какого ты города?")
    await message.answer("Имей в виду, что если ты хочешь заниматься очно, лучше указывать название ближайшего крупного населенного пункта.")
    await state.set_state(Bot_state.ask_city)


@router.message(F.text, StateFilter(Bot_state.ask_city), CityExist())
async def city_process(message: Message, state: FSMContext):
    await message.answer(f"Город {message.text} таймзона {get_timezone(message.text)}")
    await state.update_data(city=message.text)
    await state.update_data(tz=get_timezone(message.text))

    await message.answer("Какую форму обучения ты бы предпочел?", reply_markup=education_form_kb())

    await state.set_state(Bot_state.ask_education_form)


@router.message(F.text, StateFilter(Bot_state.ask_city), ~CityExist())
async def city_process_unknown_city(message: Message, state: FSMContext):
    await state.update_data(city=message.text)
    await message.answer(f"Ты ввел город {message.text}")
    await message.answer("Мы не знаем такого города. Ты уверен, что правильно ввел?", reply_markup=yesno_kb())

    await state.set_state(Bot_state.city_confirm)


@router.message(F.text == 'Да', StateFilter(Bot_state.city_confirm))
async def city_process(message: Message, state: FSMContext):
    data = await state.get_data()

    await message.answer(f"Ты ввел город {data['city']}")
    await message.answer("Пожалуйста выберете таймзону. Если нет нужно введите с клавиатуры в формате '+1'", reply_markup=timezone_kb())

    await state.set_state(Bot_state.invalid_tz)


@router.message(F.text, StateFilter(Bot_state.invalid_tz))
async def tz_process(message: Message, state: FSMContext):
    await state.update_data(tz=int(message.text))
    data = await state.get_data()

    await message.answer(f"Ты ввел город {data['city']}, таймзона {data['tz']}")
    await message.answer("Какую форму обучения ты бы предпочел?", reply_markup=education_form_kb())

    await state.set_state(Bot_state.ask_education_form)


@router.message(F.text == 'Нет', StateFilter(Bot_state.city_confirm))
async def city_process(message: Message, state: FSMContext):
    await message.answer("Из какого ты города?")

    await state.set_state(Bot_state.ask_city)


@router.message(F.text.in_(['Очная', 'Онлайн']), StateFilter(Bot_state.ask_education_form))
async def form_process(message: Message, state: FSMContext):
    await state.update_data(form=message.text)
    data = await state.get_data()
    data = data.get('time', {})
    await message.answer('Выбор времени обучения.', reply_markup=get_time_kb(data))

    await state.set_state(Bot_state.education_time)


async def update_kb(message: types.Message, user_data):
    with suppress(TelegramBadRequest):
        await message.edit_text(
            f"Календарь",
            reply_markup=get_time_kb(user_data)
        )


@router.callback_query(TimeCallbackFactory.filter(F.action == "btn"))
async def callbacks_num_change_fab(callback: types.CallbackQuery, callback_data: TimeCallbackFactory, state: FSMContext):
    data = await state.get_data()
    data = data.get('time', {})
    day = callback_data.day
    time = callback_data.time
    if f'{day}_{time}' in data:
        data.pop(f'{day}_{time}')
    else:
        data[f'{day}_{time}'] = 1

    await state.update_data(time=data)
    await update_kb(callback.message, data)


@router.callback_query(TimeCallbackFactory.filter(F.action == "done"))
async def callbacks_num_change_fab(callback: types.CallbackQuery, callback_data: TimeCallbackFactory, state: FSMContext):
    await callback.message.answer('Сохранено')
    data = await state.get_data()

    await callback.message.answer('Регистрация завершена, мы свяжемся с Вами, для дальнейшего обучения.')

    form = format_to_enum(data['form'])
    status = CandidateStatus.WAITING
    candidate = Candidate(age=data['age'], city=data['city'], gmt=data['tz'], format=form, status=status)
    user = User(tg_name=data['tg_name'], name=data['name'], user_candidate=candidate)
    await user_repo.add_candidate(user)
