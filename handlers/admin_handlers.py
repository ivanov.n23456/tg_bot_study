from aiogram.filters import Command, Filter, StateFilter
from aiogram.fsm.context import FSMContext

from database.dataclass import User, Question, Option
from database.repository import user_repo, question_repo
from states.states import Bot_state, AddTeacher, CreateQuestion

from aiogram import Router, F
from aiogram.types import Message

from filters.filters import IsAdmin
from keyboards.keyboards import set_role_kb, exit_kb

router: Router = Router()
router.message.filter(IsAdmin())


@router.message(Command(commands='admin'))
async def send_echo(message: Message):
    await message.answer('Поздравляю, ты админ')


@router.message(Command(commands='add_new'))
async def send_echo(message: Message, state: FSMContext):
    await message.answer('Пришли имя пользователя, которого ты хочешь выдать новые права')
    await message.answer('Имя пользователя вида @SomeName так же, как оно укзанно в тг')
    await state.set_state(Bot_state.education_time)


@router.message(StateFilter(Bot_state.ask_name_new_role))
async def send_echo(message: Message, state: FSMContext):
    username = message.text
    if username[0] == '@':
        username = username[1:]

    await state.update_data(username_tmp=username)
    await message.answer(f'Какую роль ты хочешь предоставить для {username}', reply_markup=set_role_kb())
    await state.set_state(Bot_state.ask_new_role)


#TODO Надо добавить еще 1 итерацию, првоерить вверно ли введен логин.
@router.message(StateFilter(Bot_state.ask_new_role))
async def send_echo(message: Message, state: FSMContext):
    data = await state.get_data()
    username = data['username_tmp']
    new_role = message.text
    await message.answer(f'Новая роль {username} предоставлена. Теперь он {new_role}')


@router.message(Command(commands='add_teacher'))
async def add_teacher_start(message: Message, state: FSMContext):
    await message.answer('Для добавления преподавателя необходимо ввести его ник в ТГ')
    await state.set_state(AddTeacher.ask_tg_name)


@router.message(F.text, StateFilter(AddTeacher.ask_tg_name))
async def add_teacher_tg_name(message: Message, state: FSMContext):
    await state.update_data(tg_name=message.text)
    await message.answer('Отлично, осталось добавить настоящее имя преподавателя, напиши его в чат')
    await state.set_state(AddTeacher.ask_name)


@router.message(F.text, StateFilter(AddTeacher.ask_name))
async def add_teacher_name(message: Message, state: FSMContext):
    data = await state.get_data()
    user = User(tg_name=data.get('tg_name'), name=message.text)
    await message.answer(f'Отлично, добавил нового преподавателя: {message.text}')
    await user_repo.add_teacher(user)
    await state.clear()


@router.message(Command(commands='add_question'))
async def create_question_start(message: Message, state: FSMContext):
    await state.clear()
    await message.answer('Ты решил создать опрос по КГ, для этого отправь текст вопроса мне')
    await state.set_state(CreateQuestion.ask_question_text)


@router.message(F.text, StateFilter(CreateQuestion.ask_question_text))
async def create_question_set_text(message: Message, state: FSMContext):
    await state.update_data(text=message.text)
    await message.answer('Вопрос запомнил, теперь отправь текст варианта ответа')
    await state.set_state(CreateQuestion.ask_option_text)


@router.message(F.text.lower() == 'завершить', StateFilter(CreateQuestion.ask_option_text))
async def create_question_finish(message: Message, state: FSMContext):
    data = await state.get_data()
    await message.answer('Сохранил опрос по КГ')
    question = Question(
        text=data.get('text'),
        question_options=[Option(text=option_text) for option_text in data.get('options')]
    )
    await question_repo.add_question(question)
    await state.clear()


@router.message(F.text, StateFilter(CreateQuestion.ask_option_text))
async def create_question_add_option(message: Message, state: FSMContext):
    data = await state.get_data()
    options = data.get('options', [])
    options.append(message.text)
    await state.update_data(options=options)
    await message.answer('Ответ к опросу добавлен')
    await message.answer(
        'Когда закончишь перечислять варианты ответов, нажми или напиши завершить',
        reply_markup=exit_kb()
    )
