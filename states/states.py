from aiogram.filters.state import State, StatesGroup


class Bot_state(StatesGroup):
    ask_name = State()
    ask_age = State()
    ask_city = State()
    city_confirm = State()
    invalid_tz = State()
    ask_education_form = State()
    education_time = State()

    ask_name_new_role = State()
    ask_new_role = State()


class AddTeacher(StatesGroup):
    ask_tg_name = State()
    ask_name = State()


class CreateQuestion(StatesGroup):
    ask_question_text = State()
    ask_option_text = State()
