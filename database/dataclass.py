from collections import namedtuple
from enum import Enum


class Day(Enum):
    MON = 1
    TUE = 2
    WEN = 3
    THU = 4
    FRI = 5
    SAT = 6
    SUN = 7


class CandidatesGroupStatus(Enum):
    RECRUITING = 1
    STUDYING = 2
    FINISHED = 3


class Format(Enum):
    LIVE = 1
    ONLINE = 2


class CandidateStatus(Enum):
    WAITING = 1
    STUDYING = 2
    LOST_CONTACT = 3
    SUSPENDED = 4


class Role(Enum):
    USER = 1
    CANDIDATE = 2
    TEACHER = 3
    ADMIN = 4


User = namedtuple('User', ['tg_name', 'name', 'user_teacher', 'user_recruiter', 'user_candidate', 'user_roles'],
                  defaults=[None] * 6)
Teacher = namedtuple('Teacher', ['teacher_user', 'teacher_cgs'],
                     defaults=[None] * 2)
Recruiter = namedtuple('Recruiter', ['recruiter_user', 'recruiter_candidates'],
                       defaults=[None] * 2)
CandidatesGroup = namedtuple('CandidatesGroup', ['tg_name', 'first_lesson', 'day', 'time', 'status', 'link_log', 'info',
                                                 'cg_candidates', 'cg_teachers'],
                             defaults=[None] * 9)
Candidate = namedtuple('Candidate', ['age', 'gmt', 'city', 'format', 'status', 'info', 'candidate_cg',
                                     'candidate_recruiter', 'candidate_user', 'candidate_skills', 'candidate_practices',
                                     'candidate_options'],
                       defaults=[None] * 12)
Skill = namedtuple('Skill', ['id', 'name', 'skill_candidates'],
                   defaults=[None] * 3)
Practice = namedtuple('Practice', ['id', 'name', 'practice_candidates'],
                      defaults=[None] * 3)
Question = namedtuple('Question', ['id', 'text', 'question_options'],
                      defaults=[None] * 3)
Option = namedtuple('Option', ['id', 'text', 'option_question', 'option_candidates'],
                    defaults=[None] * 4)
