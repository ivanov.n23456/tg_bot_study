from sqlalchemy.orm import declarative_base, mapped_column, relationship
from sqlalchemy import String, Integer, TEXT, CheckConstraint, Enum, ForeignKey
from database.dataclass import Day, CandidatesGroupStatus, Format, CandidateStatus, Role
import enum

SHORT_STR_SIZE = 30
LONG_STR_SIZE = 50

Base = declarative_base()


class UserModel(Base):
    __tablename__ = 'user'

    tg_name = mapped_column(String(SHORT_STR_SIZE), nullable=False, primary_key=True)
    name = mapped_column(String(SHORT_STR_SIZE), nullable=False)

    user_teacher = relationship('TeacherModel', uselist=False, back_populates='teacher_user')
    user_recruiter = relationship('RecruiterModel', uselist=False,back_populates='recruiter_user')
    user_candidate = relationship('CandidateModel', uselist=False, back_populates='candidate_user')
    user_roles = relationship('UserRoleModel', uselist=True, back_populates='user_role_user')


class TeacherModel(Base):
    __tablename__ = 'teacher'

    tg_name = mapped_column(ForeignKey('user.tg_name', onupdate='cascade', ondelete='cascade'), nullable=False,
                            primary_key=True)

    teacher_user = relationship('UserModel', uselist=False, back_populates='user_teacher')
    teacher_cgs = relationship('TeacherCandidatesGroupModel', uselist=True, back_populates='teacher_cg_teacher')


class RecruiterModel(Base):
    __tablename__ = 'recruiter'

    tg_name = mapped_column(ForeignKey('user.tg_name', onupdate='cascade', ondelete='cascade'), nullable=False,
                            primary_key=True)

    recruiter_candidates = relationship('CandidateModel', uselist=True, back_populates='candidate_recruiter')
    recruiter_user = relationship('UserModel', uselist=False, back_populates='user_recruiter')


class CandidatesGroupModel(Base):
    __tablename__ = 'cg'

    tg_name = mapped_column(String(LONG_STR_SIZE), nullable=False, primary_key=True)
    first_lesson = mapped_column(String(10), CheckConstraint("first_lesson LIKE '____-__-__'"), nullable=False)
    day = mapped_column(Enum(Day), nullable=False)
    time = mapped_column(String(5), CheckConstraint("time LIKE '__:__'"), nullable=False)
    status = mapped_column(Enum(CandidatesGroupStatus), nullable=False)
    link_log = mapped_column(TEXT, nullable=True)
    info = mapped_column(TEXT, nullable=True)

    cg_candidates = relationship('CandidateModel', uselist=True, back_populates='candidate_cg')
    cg_teachers = relationship('TeacherCandidatesGroupModel', uselist=True, back_populates='teacher_cg_cg')


class CandidateModel(Base):
    __tablename__ = 'candidate'

    tg_name = mapped_column(ForeignKey('user.tg_name', onupdate='cascade', ondelete='cascade'), nullable=False,
                            primary_key=True)
    age = mapped_column(Integer, nullable=False)
    gmt = mapped_column(Integer, nullable=False)
    city = mapped_column(TEXT, nullable=False)
    format = mapped_column(Enum(Format), nullable=False)
    status = mapped_column(Enum(CandidateStatus), nullable=False)
    cg_tg_name = mapped_column(ForeignKey('cg.tg_name', onupdate='cascade', ondelete='set null'), nullable=True)
    recruiter_tg_name = mapped_column(ForeignKey('recruiter.tg_name', onupdate='cascade', ondelete='set null'),
                                      nullable=True)
    info = mapped_column(TEXT, nullable=True)

    candidate_cg = relationship('CandidatesGroupModel', uselist=False, back_populates='cg_candidates')
    candidate_recruiter = relationship('RecruiterModel', uselist=False, back_populates='recruiter_candidates')
    candidate_user = relationship('UserModel', uselist=False, back_populates='user_candidate')
    candidate_skills = relationship('CandidateSkillModel', uselist=True, back_populates='candidate_skill_candidate')
    candidate_practices = relationship('CandidatePracticeModel', uselist=True,
                                       back_populates='candidate_practice_candidate')
    candidate_options = relationship('AnswerModel', uselist=True, back_populates='answer_candidate')


class SkillModel(Base):
    __tablename__ = 'skill'

    id = mapped_column(Integer, nullable=False, primary_key=True, autoincrement=True)
    name = mapped_column(TEXT, nullable=False)

    skill_candidates = relationship('CandidateSkillModel', uselist=True, back_populates='candidate_skill_skill')


class CandidateSkillModel(Base):
    __tablename__ = 'candidate_skill'

    id = mapped_column(ForeignKey('skill.id', onupdate='cascade', ondelete='cascade'), nullable=False, primary_key=True)
    tg_name = mapped_column(ForeignKey('candidate.tg_name', onupdate='cascade', ondelete='cascade'), nullable=False,
                            primary_key=True)

    candidate_skill_skill = relationship('SkillModel', uselist=False, back_populates='skill_candidates')
    candidate_skill_candidate = relationship('CandidateModel', uselist=False, back_populates='candidate_skills')


class PracticeModel(Base):
    __tablename__ = 'practice'

    id = mapped_column(Integer, nullable=False, primary_key=True, autoincrement=True)
    name = mapped_column(TEXT, nullable=False)

    practice_candidates = relationship('CandidatePracticeModel', uselist=True,
                                       back_populates='candidate_practice_practice')


class CandidatePracticeModel(Base):
    __tablename__ = 'candidate_practice'

    id = mapped_column(ForeignKey('practice.id', onupdate='cascade', ondelete='cascade'), nullable=False,
                       primary_key=True)
    tg_name = mapped_column(ForeignKey('candidate.tg_name', onupdate='cascade', ondelete='cascade'), nullable=False,
                            primary_key=True)

    candidate_practice_practice = relationship('PracticeModel', uselist=False, back_populates='practice_candidates')
    candidate_practice_candidate = relationship('CandidateModel', uselist=False, back_populates='candidate_practices')


class UserRoleModel(Base):
    __tablename__ = 'user_role'

    tg_name = mapped_column(ForeignKey('user.tg_name', onupdate='cascade', ondelete='cascade'), nullable=False,
                            primary_key=True)
    role = mapped_column(Enum(Role), nullable=False, primary_key=True)

    user_role_user = relationship('UserModel', uselist=False, back_populates='user_roles')


class TeacherCandidatesGroupModel(Base):
    __tablename__ = 'teacher_cg'

    teacher_tg_name = mapped_column(ForeignKey('teacher.tg_name', onupdate='cascade', ondelete='cascade'),
                                    nullable=False, primary_key=True)
    cg_tg_name = mapped_column(ForeignKey('cg.tg_name', onupdate='cascade', ondelete='cascade'), nullable=False,
                               primary_key=True)

    teacher_cg_teacher = relationship('TeacherModel', uselist=False, back_populates='teacher_cgs')
    teacher_cg_cg = relationship('CandidatesGroupModel', uselist=False, back_populates='cg_teachers')


class QuestionModel(Base):
    __tablename__ = 'question'

    id = mapped_column(Integer, nullable=False, primary_key=True, autoincrement=True)
    text = mapped_column(TEXT, nullable=False)

    question_options = relationship('OptionModel', uselist=True, back_populates='option_question')


class OptionModel(Base):
    __tablename__ = 'option'

    id = mapped_column(Integer, nullable=False, primary_key=True, autoincrement=True)
    question_id = mapped_column(ForeignKey('question.id', onupdate='cascade', ondelete='cascade'), nullable=False)
    text = mapped_column(TEXT, nullable=False)

    option_question = relationship('QuestionModel', uselist=False, back_populates='question_options')
    option_candidates = relationship('AnswerModel', uselist=True, back_populates='answer_option')


class AnswerModel(Base):
    __tablename__ = 'answer'

    tg_name = mapped_column(ForeignKey('candidate.tg_name', onupdate='cascade', ondelete='cascade'), nullable=False,
                            primary_key=True)
    option_id = mapped_column(ForeignKey('option.id', onupdate='cascade', ondelete='cascade'), nullable=False,
                              primary_key=True)

    answer_candidate = relationship('CandidateModel', uselist=False, back_populates='candidate_options')
    answer_option = relationship('OptionModel', uselist=False, back_populates='option_candidates')
