from typing import Optional, List
from sqlalchemy import select
from sqlalchemy.orm import selectinload
from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker
from database.dataclass import *
from database.model import *


class BaseRepository:
    engine = create_async_engine('sqlite+aiosqlite:///:memory:', echo=True)

    def __init__(self):
        self._session_maker = async_sessionmaker(bind=BaseRepository.engine)


class UserRepository(BaseRepository):
    async def check_if_exists(self, tg_name: str) -> bool:
        async with self._session_maker() as session:
            user_model = await session.get(UserModel, tg_name)
        return user_model is not None

    #TODO механизм начального админа. А то как раздовать роли, если никто не проходит фильтр на админа в начале.
    async def get_roles(self, tg_name: str) -> Optional[List[Role]]:
        async with self._session_maker() as session:
            user_model_res = await session.execute(select(UserModel)
                                                   .where(UserModel.tg_name == tg_name)
                                                   .options(selectinload(UserModel.user_roles)))
            user_model = user_model_res.one_or_none()
        if user_model is not None:
            return [i.role for i in user_model[0].user_roles]
        return None

    async def add_candidate(self, user: User) -> None:
        async with self._session_maker() as session:
            user_model = UserModel(tg_name=user.tg_name, name=user.name)
            role_models = [UserRoleModel(role=Role.USER), UserRoleModel(role=Role.CANDIDATE)]
            user_model.user_roles.extend(role_models)
            candidate_model = CandidateModel(age=user.user_candidate.age, city=user.user_candidate.city,
                                             gmt=user.user_candidate.gmt, format=user.user_candidate.format,
                                             status=user.user_candidate.status,
                                             cg_tg_name=None,
                                             recruiter_tg_name=None)
            user_model.user_candidate = candidate_model
            session.add(user_model)
            await session.commit()

    async def add_teacher(self, user):
        async with self._session_maker() as session:
            user_model = UserModel(tg_name=user.tg_name, name=user.name)
            role_models = [UserRoleModel(role=Role.USER), UserRoleModel(role=Role.TEACHER)]
            user_model.user_roles.extend(role_models)
            teacher_model = TeacherModel(tg_name=user.tg_name)
            user_model.user_teacher = teacher_model
            session.add(user_model)
            await session.commit()


class CandidatesGroupRepository(BaseRepository):
    pass


class QuestionRepository(BaseRepository):
    async def add_question(self, question):
        async with self._session_maker() as session:
            question_model = QuestionModel(text=question.text)
            option_models = [OptionModel(text=option.text) for option in question.question_options]
            question_model.question_options = option_models
            session.add(question_model)
            await session.commit()


user_repo = UserRepository()
cg_repo = CandidatesGroupRepository()
question_repo = QuestionRepository()
