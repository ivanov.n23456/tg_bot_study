from aiogram.types import ReplyKeyboardMarkup
from aiogram.utils.keyboard import ReplyKeyboardBuilder, InlineKeyboardBuilder
from typing import Optional

from aiogram.filters.callback_data import CallbackData


def gender_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardBuilder()
    kb.button(text="Мужской")
    kb.button(text="Женский")
    kb.button(text="Затрудняюсь")
    kb.adjust(2)
    return kb.as_markup(resize_keyboard=True, one_time_keyboard=True)


def set_role_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardBuilder()
    kb.button(text="Админ")
    kb.button(text="Учитель")
    kb.button(text="Ученик")
    kb.adjust(2)
    return kb.as_markup(resize_keyboard=True, one_time_keyboard=True)


def education_form_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardBuilder()
    kb.button(text="Очная")
    kb.button(text="Онлайн")
    kb.adjust(2)
    return kb.as_markup(resize_keyboard=True, one_time_keyboard=True)


def timezone_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardBuilder()
    for i in range(2, 13):
        kb.button(text=f"+{i}")
    return kb.as_markup(resize_keyboard=True, one_time_keyboard=True)


def yesno_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardBuilder()
    kb.button(text="Да")
    kb.button(text="Нет")
    kb.adjust(2)
    return kb.as_markup(resize_keyboard=True, one_time_keyboard=True)


def list_kb(cmds) -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardBuilder()
    for cmd in cmds:
        kb.button(text=cmd)
    kb.adjust(2)
    return kb.as_markup(resize_keyboard=True, one_time_keyboard=True)


class TimeCallbackFactory(CallbackData, prefix="daychoise"):
    day: Optional[str]
    time: Optional[str]
    action: Optional[str]


def get_time_kb(user_data):
    builder = InlineKeyboardBuilder()
    days = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС']
    time = [str(i) for i in range(10, 23, 1)]
    for t in days:
        builder.button(text=t, callback_data=TimeCallbackFactory(
            action='none', day="none", time='none'))

    for t in time:
        for day in days:
            if user_data.get(f'{day}_{t}', 0) == 1:
                builder.button(text=f'({t})', callback_data=TimeCallbackFactory(
                    action='btn', day=day, time=t))
            else:
                builder.button(text=f'{t}', callback_data=TimeCallbackFactory(
                    action='btn', day=day, time=t))

    builder.button(text='Подтвердить', callback_data=TimeCallbackFactory(
        action='done', day="done", time='none'))
    builder.adjust(7)
    return builder.as_markup()


def exit_kb() -> ReplyKeyboardMarkup:
    kb = ReplyKeyboardBuilder()
    kb.button(text="Завершить")
    kb.adjust(1)
    return kb.as_markup(resize_keyboard=True, one_time_keyboard=True)
