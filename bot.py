import asyncio

from aiogram import Bot, Dispatcher
from aiogram.fsm.storage.memory import MemoryStorage

from config import API_BOT_CLIENTS
# admin_handlers, auth_user_handlers, registration
from handlers import registration, admin_handlers
from database.repository import BaseRepository
from database.model import Base

storage: MemoryStorage = MemoryStorage()


async def main():
    # Инициализируем бд, бот и диспетчер
    async with BaseRepository.engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    bot: Bot = Bot(token=API_BOT_CLIENTS, parse_mode='HTML')
    dp: Dispatcher = Dispatcher(storage=storage)

    # Регистриуем роутеры в диспетчере
    dp.include_router(registration.router)
    dp.include_router(admin_handlers.router)
    # dp.include_router(auth_user_handlers.router)

    # Пропускаем накопившиеся апдейты и запускаем polling
    await bot.delete_webhook(drop_pending_updates=True)
    await dp.start_polling(bot)


if __name__ == '__main__':
    asyncio.run(main())
