import re

from aiogram.filters import Filter
from aiogram.types import Message

from aiogram.fsm.context import FSMContext

from utils.utils import check_city, get_timezone

from database.dataclass import Role
from database.repository import user_repo


class IsAdmin(Filter):
    async def __call__(self, message: Message) -> bool:
        return Role.ADMIN in await user_repo.get_roles(message.from_user.username)


class CityExist(Filter):
    """Фильтр на проверку, существования города"""
    async def __call__(self, message: Message) -> bool:
        tz = get_timezone(message.text)
        if tz == 'unknown':
            return False
        return check_city(message.text)


class IsClient(Filter):

    async def __call__(self, message: Message) -> bool:
        return await user_repo.check_if_exists(message.from_user.username)


class IsAuthenticated(Filter):
    async def __call__(self, *args, state: FSMContext) -> bool:
        data = await state.get_data()
        print(data)
        return data.get('registred', False)
